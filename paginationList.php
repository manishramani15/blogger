<?php
  session_start();
  include_once 'config.php';
  $result = $conn->query("select count(*) as total from (" . $_SESSION['query'] .") as t");
  if ($result->num_rows > 0)
  {
    while ($row = $result->fetch_assoc())
      {
        $total = $row['total']; 
        $limit = 5;
        $numberOfPages = ceil($total/$limit);
        if($numberOfPages > 0){
          echo "<i>Go To Page: <i><br>
          <ul class='pagination' >";
        }
          for ($i=1; $i <= $numberOfPages ; $i++) {
            echo "<li><a href='pagination.php?pageNumber=";
            echo $i;
            echo "'>".$i."</a></li>";
          }
          echo "</ul>";
      }
  } else {
    echo $conn->error;
  }
?>