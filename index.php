<!DOCTYPE html>
<html>
<head>
  <title>Blogger</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <link rel="stylesheet" type="text/css" href="index.css">
  <script type="text/javascript" src="Blogger.js"></script>
</head>
<body>
  <?php
  session_start();
  $_SESSION['pageNumber'] = 1;
  $_SESSION['limit'] = 5;
  $offset = ($_SESSION['pageNumber']-1) * $_SESSION['limit'];
  if(!isset($_SESSION['limitQuery'])){
    $_SESSION['limitQuery'] = "limit " . $offset . "," . $_SESSION['limit']; 
  }
  include_once 'config.php';

  $conn = new mysqli($servername, $username, $password, $dbName);

  if ($conn->connect_error)
    {
    die("Connection failed: " . $conn->connect_error);
    }

  if (!isset($_SESSION['query']))
    {
    $_SESSION['query'] = "select articles.id, articles.date_time, authors.name, articles.content, articles.title, group_concat(categories.category_name) as categories, tag_names.tags from articles join authors on articles.authors_id = authors.id join articles_categories on articles.id = articles_categories.articles_id join categories on articles_categories.categories_id = categories.id join (select articles.id, group_concat(tags.tag_name) as tags from articles join articles_tags on articles.id = articles_tags.articles_id join tags on articles_tags.tags_id = tags.id group by articles.id) as tag_names on articles.id = tag_names.id group by articles.id order by articles.date_time desc ";
    }

  ?>
  <div class='header'>
    <div class='row'>
      <div class="col-sm-1">
        <img src="images.jpeg" style="height: 80%; width: 80%;">
      </div>
      <div class="col-sm-9" style="display: flex; justify-content: center; align-content: center;">
        <h1>Blogger's Blogspot</h1>
      </div>
      <div class="col-sm-2" class="btn-group">
        <?php

if (isset($_SESSION['name']))
  {
  echo "<h5>Welcome " . $_SESSION['name'] . "</h5>";
  echo "<form action='logout.php'>";
  echo "<button class='btn btn-primary btn-xs' type='submit' name='logout' data-toggle='modal' data-target='#logoutModal'>Logout</button>";
  echo "</form>";
  }
  else
  {
  echo "<button class='btn btn-primary' type='button' name='login' data-toggle='modal' data-target='#loginModal'>Login</button>";
  echo "<button class='btn btn-primary' type='button' name='signup' data-toggle='modal' data-target='#signUpModal'>Sign Up</button>";
  }

?>
      </div> 
    </div>
  </div>
  <div class="container">
    <div class="row" style="height: 100%;">
      <div class="col-sm-10" style="height: 100%; background-color: navajowhite;">
        <div class="search" style="width: 100%; position: relative; left: 50px; top: 20px; padding-bottom: 30px;">
          <form action='filter.php' method='get' data-type='search-form'>
            <input type="input" name="search" style="width: 80%;" placeholder="Search by writer or title" value=<?php
echo $_SESSION['searchInput']; ?>>
            <button type="submit" class="btn btn-info">
              <span class="glyphicon glyphicon-search"></span> Search
            </button>
          </form>
        </div>
        <div class="article-container" data-type='article-container'>
        <?php include 'articleContainer.php' ?>
        </div>
      </div>
      <div class="col-sm-2" style="background-color: indianred; height: 100%;">
        <div id='date'>
          <form action='filter.php' method="get" data-type='date-form'>
            <i><b><h4>Search By Date: </h4></i></b>
            <input type="date" name="date" value=<?php
echo $_SESSION['date']; ?>>
            <button type="submit" name="submit" class="glyphicon glyphicon-search" style='background-color: lightblue;'></button>
          </form>
        </div>
        <div id='categories'>
          <form action=filter.php method='get' data-type='category-form'>
            <i><b><h4>Search By Categories: </h4></i></b>
            <select name="category_name[]" multiple>
              <?php
$result = $conn->query('select category_name from categories;');

if ($result->num_rows > 0)
  {
  while ($row = $result->fetch_assoc())
    {
    echo "<option value='" . $row['category_name'] . "'";
    if (strpos($_SESSION['categories'], $row['category_name']) !== false)
      {
      echo "selected";
      }

    echo ">" . $row['category_name'] . "</option>";
    }
  }

?>    
            </select>
            <button type="submit" name="submit" class="glyphicon glyphicon-search" style='background-color: lightblue;'></button>
          </form>
        </div>
        <div id='tags'>
          <form action=filter.php method='get' data-type='tag-form'>
            <i><b><h4>Search By Tags: </h4></i></b>
            <select name="tag_name[]" multiple>
              <?php
                $result = $conn->query('select tag_name from tags;');

                if ($result->num_rows > 0)
                  {
                  while ($row = $result->fetch_assoc())
                    {
                    echo "<option value='" . $row['tag_name'] . "'";
                    if (strpos($_SESSION['tags'], $row['tag_name']) !== false)
                      {
                      echo "selected";
                      }

                    echo ">" . $row['tag_name'] . "</option>";
                    }
                  }

              ?>    
            </select>
            <button type="submit" name="submit" class="glyphicon glyphicon-search" style='background-color: lightblue;'></button>
          </form>
        </div>
        <div id="pagination" data-type='pagination-list'>
            <?php
              include_once 'paginationList.php';
            ?>
        </div>
        <div id='refresh-filters'>
          <form action='refreshFilters.php'>
            <button class="btn btn-warning" type="submit" style="color:black;"><i>Refresh Filters</i></button>
          </form>
        </div>

      </div>
    </div>
 <!-- //     Modal login -->
  <div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
          <form action="loginValidator.php" data-type="login-form" method="post">
          <!-- <form data-tag="login-form">  -->
            Email: <br />
            <input type="email" data-input-type="email" id="login-email-input" name="email" class="form-control" required><br>
            Password: <br />
            <input type="password" id="login-pwd-input" name="password" class="form-control" required><br />
            <input type="submit" name="" id="login-btn" class="btn btn-primary btn-sm" name="button" value="Login">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- // -->
   <!-- //     Modal sign up -->
  <div class="modal fade" id="signUpModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Up</h4>
        </div>
        <div class="modal-body">
          <form action="signUpValidator.php" method="post" data-tag="sign-up-form" data-type="signUp-form">
          <!-- <form data-tag="login-form">  -->
            Name: <br />
            <input type="name" id="login-name-input" name="name" class="form-control" required><br>
            Email: <br />
            <input type="email" id="login-email-input" name="email" class="form-control" data-input-type="email" required><br>
            Password: <br />
            <input type="password" id="login-pwd-input" name="password" class="form-control" data-input-type="password" required><br />
            Confirm Password: <br />
            <input type="password" id="login-pwd-input" name="confirm_password" class="form-control" data-input-type="confirm-password" required><br />
            <input type="submit" name="submit" id="login-btn" class="btn btn-primary btn-sm" name="button" value="Sign Up">
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- // -->
  </div>
  <div class='footer'> 
    <!-- <form action='writeArticle.php'> -->
      <?php

if (isset($_SESSION['name']))
  {
  echo "<button type='button' style='font-size: 30px; background-color: khaki;' data-toggle='modal' data-target='#writeArticleModal'> Write An Article </button>";
  }
  else echo "<button type='button' style='font-size: 30px; background-color: khaki;' data-toggle='modal' data-button-type='write-article'> Write An Article </button>";
?>
    <!-- writeArticleModal -->
    <div class="modal fade" id="writeArticleModal" role="dialog">
      <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Details</h4>
        </div>
        <div class="modal-body">
          <form action="writeArticle.php" enctype="multipart/form-data" method="post" data-type="article-form">
            Title: <br />
            <input type="text" name="title" class="form-control" data-input-type='title' required><br>
            Category: <br />
            <select name="category_name[]" placeholder="select category" multiple required>
              <?php
                $result = $conn->query('select category_name from categories;');

                if ($result->num_rows > 0)
                  {
                  while ($row = $result->fetch_assoc())
                    {
                    echo "<option value='" . $row['category_name'] . "'>" . $row['category_name'] . "</option>";
                    }
                  }

              ?>    
            </select> <br />
            Content: <i>(atleast 50 characters)</i> <br />
            <textarea name='textContent' id='textContent' data-input-type="textArea" required></textarea><br />
            Select images: <i>(jpeg, gif, png)</i> <input type="file" name="img[]" multiple><br />
            Tags <i>(seperated by ',')</i>: <br />
            <input type="text" name="tags" class="form-control" data-input-type="tag-input"><br />
            <input type="submit" name="submit" class="btn btn-primary btn-sm" value="Submit Article"><br>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      </div>
    </div>
  <!-- e -->
  </div>
  <?php

if(isset($_SESSION['fileTypeError'])) {
  echo "<script> alert('Image file type not supported. Image should be of the format jpeg/gif/png.');</script>";
  unset($_SESSION['fileTypeError']);
}

if (isset($_SESSION['loginError']))
  {

  echo "<script> $('#loginModal').modal('show'); </script>";

  echo "<script> alert('Password is incorrect.');</script>";
  unset($_SESSION['loginError']);
  }

if (isset($_SESSION['signUpError']))
  {
  echo "<script> $('#signUpModal').modal('show'); </script>";
  echo "<script> alert('SignUp Failed.');</script>";
  unset($_SESSION['signUpError']);
  }

if (isset($_SESSION['writeArticleError']))
  {
  echo "<script> $('#writeArticleModal').modal('show'); </script>";
  echo "<script> alert('Upload article failed.');</script>";
  unset($_SESSION['writeArticleError']);
  }
?>
</body>
</html>