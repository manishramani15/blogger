<?php
session_start();
$pageNumber = $_GET['pageNumber'];
if($pageNumber) {
  $_SESSION['pageNumber'] = $pageNumber;
}
$offset = ($_SESSION['pageNumber']-1) * $_SESSION['limit'];

$_SESSION['limitQuery'] = "limit " . $offset . "," . $_SESSION['limit'];
include 'articleContainer.php';
?>