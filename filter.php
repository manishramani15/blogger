<?php
session_start();
include_once 'config.php';
$date = $_GET['date'];
$searchInput = $_GET['search'];
$categoryInput =  $_GET['category_name'];
$tagInput = $_GET['tag_name'];

if($searchInput !== NULL) {
  $_SESSION['searchInput'] = $searchInput;
}

if($date) {
  $_SESSION['date'] = $date;
}

if($categoryInput) {
  $_SESSION['categories'] = implode("', '", $_GET['category_name']);
}

if($tagInput) {
  $_SESSION['tags'] = implode("', '", $_GET['tag_name']);
}

$_SESSION['query'] = "select articles.date_time, articles.content, articles.id, authors.name, articles.title, group_concat(categories.category_name) as categories, tag_names.tags from articles join authors on articles.authors_id = authors.id join articles_categories on articles.id = articles_categories.articles_id join categories on articles_categories.categories_id = categories.id join (select articles.id, group_concat(tags.tag_name) as tags from articles join articles_tags on articles.id = articles_tags.articles_id join tags on articles_tags.tags_id = tags.id ";

if($_SESSION['tags']) {
  $_SESSION['query'] .= "where tags.tag_name in ('".$_SESSION['tags']."') ";
}
  $_SESSION['query'] .= "group by articles.id) as tag_names on articles.id = tag_names.id ";

if($_SESSION['categories']) {
  $_SESSION['query'] .= "where categories.category_name in ('".$_SESSION['categories']."') ";
}
  
  $_SESSION['query'] .= "group by articles.id having (articles.title like '%".$_SESSION['searchInput']."%' OR authors.name like '%".$_SESSION['searchInput']."%')";

if($_SESSION['date']) {
  $_SESSION['query'] .= " AND date(articles.date_time) = '".$_SESSION['date']."'";
}
  
  $_SESSION['query'] .= " order by articles.date_time desc " ;
  unset($_SESSION['limitQuery']);
  include 'articleContainer.php';

?>