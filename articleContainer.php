<?php
session_start();
include_once 'config.php';
$articles = $conn->query($_SESSION['query'] . $_SESSION['limitQuery']);
if ($articles->num_rows > 0)
  {
  while ($row = $articles->fetch_assoc())
    {
    $date = getdate(strtotime($row['date_time']));
    $date = "$date[hours]:$date[minutes], $date[month] $date[mday], $date[year]";
    echo "<div class='container-fluid' data-type='articleView'>
                            <div class='card'>
                             <div class='card-body'>
                               <h6 class='pull-right'>" . $date . "</h6>
                               <h1 class='display-4'><b>" . $row['title'] . "</b></h1>
                               <h4><i>Belongs To: " . $row['categories'] . "</i></h4>";
    if ($row['tags'])
      {
      echo "<h4><i>Tags: " . $row['tags'] . "</i></h4>";
      }

    echo "<h3><i>Written By: <a href='filterByAuthor.php?name=".$row['name']."' data-type='filer-by-author'>" . $row['name'] . "</a></i></h3>
                               <p class='lead'>" . $row['content'] . "</p>";
          $images = $conn->query("select image_path from images where articles_id = " . $row['id']);
          if ($images->num_rows > 0)
            {
            echo "<div class='img-container'>";
            while ($img_row = $images->fetch_assoc())
              {
              echo "<div><img src=" . $img_row['image_path'] . "></div>";
              }

            echo "</div>";
            }

    $comments = $conn->query("select date_time, commentator_email, commentator_name, content from comments where articles_id =" . $row['id'] . " order by id desc");
    echo "<div class='comments'>";
    if ($comments->num_rows > 0)
      {
      while ($comment_row = $comments->fetch_assoc())
        {
        $commentDate = getdate(strtotime($comment_row['date_time']));
        $commentDate = "$commentDate[hours]:$commentDate[minutes], $commentDate[month] $commentDate[mday], $commentDate[year]";
        echo "<br /><div class='comment'>
                            <div class='comment-header'>
                              <span class='pull-right'><i>Commented On: " . $commentDate . "</i></span>
                              <span class='commentator-name'><i><b>Commented By: </b>" . $comment_row['commentator_name'] . "</i></span>
                              <span>(" . $comment_row['commentator_email'] . ")</span>
                            </div>
                            <div class='comment-body'><b>Comment: </b>
                            <i>" . $comment_row['content'] . "</i>
                            </div>
                          </div><br />";
        }
      }

    echo "</div></div>
                           </div>
                          </div>";
    if ($_SESSION['id'])
      {
      echo "<button data-toggle='collapse' class='btn btn-link' data-target='#write_comment_" . $row['id'] . "'>Leave a Comment</button>
                  <div id='write_comment_" . $row['id'] . "' class='collapse'>
                    <div class='formContainer'>
                      <form action='commentLoader.php' method='post'>
                          <input type='hidden' name='article_id' value='" . $row['id'] . "'>
                        <i><b>Comment: </b></i><br />
                          <textarea name='commentInput' style='width:80%;' required></textarea>
                          <input type='submit' value='Submit Comment'>
                          </form>
                    </div>
                  </div> ";
      }
      else
      {
      echo "<button data-toggle='collapse' class='btn btn-link' data-target='#write_comment_" . $row['id'] . "'>Leave a Comment</button>
                  <div id='write_comment_" . $row['id'] . "' class='collapse'>
                    <div class='formContainer'>
                      <form action='commentLoader.php' method='post'>
                        Name: <br />
                          <input type='name' name='name' style='width:80%;' required> <br />
                        Email: <br />
                          <input type='email' name='email' style='width:80%;' required> <br />
                          <input type='hidden' name='article_id' value='" . $row['id'] . "'>
                        <i><b>Comment: </b></i><br />
                          <textarea name='commentInput' style='width:80%;' required></textarea>
                          <input type='submit' value='Submit Comment'>
                          </form>
                    </div>
                  </div> ";
      }
    }
  }
  else
  {
  $_SESSION['loadError'] = true;
  }
?>