function Blogger(options) {
  this.signUpForm = options.signUpForm;
  this.articleForm = options.articleForm;
  this.writeArticleButton = options.writeArticleButton;
  this.articleView = options.articleView;
  this.searchForm = options.searchForm;
  this.dateForm = options.dateForm;
  this.categoryForm = options.categoryForm;
  this.tagForm = options.tagForm;
  this.articleContainer = options.articleContainer;
  this.paginationList = options.paginationList;
  this.authorNameAnchor = options.authorNameAnchor;
  this.loginForm = options.loginForm;
}

Blogger.prototype.init = function() {
  this.bindEvents();
};

Blogger.prototype.bindEvents = function() {
  var _this = this;
  this.signUpForm.on('submit', this.checkSignUpFields.bind(this));
  this.articleForm.on('focusout', '[data-input-type="textArea"]', this.checkArticleFields.bind(this));
  this.articleForm.on('focusout', '[data-input-type="title"]', this.articleTitleHandler.bind(this));
  this.writeArticleButton.on('click', this.alertToLogin.bind(this));
  this.articleContainer.on('click', this.articleView, this.markClassCurrent.bind(this));
  this.signUpForm.on('focusout', '[data-input-type="email"]', this.signUpAjaxHandler.bind(this));
  this.loginForm.on('focusout', '[data-input-type="email"]', this.loginAjaxHandler.bind(this));
  this.searchForm.on('submit', this.searchHandler.bind(this));
  this.dateForm.on('submit', this.dateHandler.bind(this));
  this.categoryForm.on('submit', this.categoryHandler.bind(this));
  this.tagForm.on('submit', this.tagHandler.bind(this));
  this.paginationList.on('click', 'li', function(event) {
    event.preventDefault();
    var pageNumber = $(this).text();
    _this.goToPageNumber(pageNumber);
  });
  this.articleContainer.on('click', this.authorNameAnchor, this.authorViewHandler.bind(this));
};

Blogger.prototype.articleTitleHandler = function(event) {
  var _this = this;
  $.ajax({
      url: "checkTitleExist.php?title=" + _this.articleForm.find('[data-input-type="title"]').val(),
      type: "GET",
    })
    .done(function(response) {
      if (response === 'exists') {
        _this.articleForm.find('[data-text="title-text"]').remove();
        _this.articleForm.find('[data-input-type="title"]').css('border-color', 'red')
          .after('<span class="glyphicon glyphicon-remove" style="color: red;" data-text="title-text"> Article by the same title already exists! </span>');
        _this.articleForm.find('input[type="submit"]').attr('disabled', 'disabled');

      } else {
        _this.articleForm.find('[data-input-type="title"]').css('border-color', '');
        _this.articleForm.find('[data-text="title-text"]').removeClass("glyphicon glyphicon-remove", "glyphicon glyphicon-ok").text("").css('color','green').addClass("glyphicon glyphicon-ok");
        _this.articleForm.find('input[type="submit"]').removeAttr('disabled');
      }
    });
}

Blogger.prototype.authorViewHandler = function(event) {
  var _this = this;
  event.preventDefault();
  console.log($(event.target).text());
  $.ajax({
      url: "filterByAuthor.php?name=" + $(event.target).text(),
      type: "GET"
    })
    .done(function(response) {
      console.log(response);
      _this.articleContainer.empty().append(response);
      console.log("goToPageNumber");
      _this.paginationAjaxHandler();
      _this.goToPageNumber(1);
    });

}

Blogger.prototype.goToPageNumber = function(pageNumber) {
  var _this = this;
  $.ajax({
      url: "pagination.php?pageNumber=" + pageNumber,
      type: "GET"
    })
    .done(function(response) {
      console.log(response);
      _this.articleContainer.empty().append(response);
      console.log("goToPageNumber");
    });
}

Blogger.prototype.tagHandler = function(event) {
  var serializedData = this.tagForm.serialize();
  event.preventDefault();
  this.filterAjaxHandler(serializedData);
};

Blogger.prototype.categoryHandler = function(event) {
  var serializedData = this.categoryForm.serialize();
  event.preventDefault();
  this.filterAjaxHandler(serializedData);
};

Blogger.prototype.dateHandler = function(event) {
  var serializedData = this.dateForm.serialize();
  event.preventDefault();
  this.filterAjaxHandler(serializedData);
};

Blogger.prototype.searchHandler = function(event) {
  var serializedData = this.searchForm.serialize();
  event.preventDefault();
  this.filterAjaxHandler(serializedData);
};

Blogger.prototype.paginationAjaxHandler = function() {
  $.ajax({
      url: "paginationList.php"
    })
    .done(function(response) {
      console.log("paginationAjaxHandler");
      console.log(response);
      $('[data-type="pagination-list"').empty().append(response);
    });
}

Blogger.prototype.filterAjaxHandler = function(serializedData) {
  var _this = this;
  $.ajax({
      url: "filter.php",
      type: "GET",
      data: serializedData
    })
    .done(function(response) {
      console.log(response);
      _this.articleContainer.empty().append(response);
      _this.paginationAjaxHandler();
      _this.goToPageNumber(1);
    });
};

Blogger.prototype.signUpAjaxHandler = function(event) {
  var _this = this;
  $.ajax({
      url: "checkEmailExist.php?email=" + _this.signUpForm.find('[data-input-type="email"]').val(),
      type: "GET",
    })
    .done(function(response) {
      if (response === 'exists') {
        _this.signUpForm.find('[data-text="email-text"]').remove();
        _this.signUpForm.find('[data-input-type="email"]').css('border-color', 'red')
          .after('<span class="glyphicon glyphicon-remove" style="color: red;" data-text="email-text"> Account from this email (' + _this.signUpForm.find('[data-input-type="email"]').val() + ') already exists!</span>');
        _this.signUpForm.find('input[type="submit"]').attr('disabled', 'disabled');

      } else {
        _this.signUpForm.find('[data-input-type="email"]').css('border-color', '');
        _this.signUpForm.find('[data-text="email-text"]').removeClass("glyphicon glyphicon-remove").text("").css('color','green').addClass("glyphicon glyphicon-ok");
        _this.signUpForm.find('input[type="submit"]').removeAttr('disabled');

      }
    });
};

Blogger.prototype.loginAjaxHandler = function(event) {
  var _this = this;
  $.ajax({
      url: "checkEmailExist.php?email=" + _this.loginForm.find('[data-input-type="email"]').val(),
      type: "GET",
    })
    .done(function(response) {
      if (response === 'not exists') {
        _this.loginForm.find('[data-text="email-text"]').remove();
        _this.loginForm.find('[data-input-type="email"]').css('border-color', 'red')
          .after('<span class="glyphicon glyphicon-remove" style="color: red;" data-text="email-text"> Account from this email (' + _this.loginForm.find('[data-input-type="email"]').val() + ') does not exists!</span>');
        _this.loginForm.find('input[type="submit"]').attr('disabled', 'disabled');

      } else {
        _this.loginForm.find('[data-input-type="email"]').css('border-color', '');
        _this.loginForm.find('[data-text="email-text"]').removeClass("glyphicon glyphicon-remove").text("").css('color','green').addClass("glyphicon glyphicon-ok");
        _this.loginForm.find('input[type="submit"]').removeAttr('disabled');
      }
    });
};

Blogger.prototype.markClassCurrent = function(event) {
  $(event.target).closest(this.articleView).addClass("currentArticle").siblings().removeClass("currentArticle");
  $(event.target).closest(this.articleView)[0].scrollIntoView(true);
}

Blogger.prototype.alertToLogin = function() {
  alert('You must Login or Sign Up to write an article.');
};

Blogger.prototype.checkArticleFields = function(event) {
  var textAreaLength = this.articleForm.find('[data-input-type="textArea"]').val().length;
  if (textAreaLength < 50) {
    alert("Content should be atleast 50 characters");
    this.articleForm.find('input[type="submit"]').attr('disabled', 'disabled');
    event.preventDefault();
  } else {
    this.articleForm.find('input[type="submit"]').removeAttr('disabled');  
  }
};

Blogger.prototype.checkSignUpFields = function(event) {
  var confirmPassword = this.signUpForm.find('[data-input-type="confirm-password"]').val(),
    password = this.signUpForm.find('[data-input-type="password"]').val();
  if (password != confirmPassword) {
    alert("Password does not match");
    event.preventDefault();
  }
};

$(function() {
  var options = {
    signUpForm: $('[data-type="signUp-form"]'),
    articleForm: $('[data-type="article-form"]'),
    writeArticleButton: $('[data-button-type="write-article"]'),
    articleView: '[data-type="articleView"]',
    searchForm: $('[data-type="search-form"]'),
    dateForm: $('[data-type="date-form"]'),
    categoryForm: $('[data-type="category-form"]'),
    tagForm: $('[data-type="tag-form"]'),
    articleContainer: $('[data-type="article-container"]'),
    paginationList: $('[data-type="pagination-list"]'),
    authorNameAnchor: '[data-type="filer-by-author"]',
    loginForm : $('[data-type="login-form"]'),
  }
  blogger = new Blogger(options);
  blogger.init();
})